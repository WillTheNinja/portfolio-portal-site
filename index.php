<?php
	use Portal\MinecraftPing;
	use Portal\MinecraftPingException;
	define( 'MQ_SERVER_ADDR', 'Hub.TheCrafters.net' );
	define( 'MQ_SERVER_PORT', 25565 );
	define( 'MQ_TIMEOUT', 1 );
	Error_Reporting( E_ALL | E_STRICT );
	Ini_Set( 'display_errors', true );
	require __DIR__ . '/Landing/MinecraftPing.php';
	require __DIR__ . '/Landing/MinecraftPingException.php';
	$Timer = MicroTime( true );
	$Info = false;
	$Query = null;
	try{
		$Query = new MinecraftPing( MQ_SERVER_ADDR, MQ_SERVER_PORT, MQ_TIMEOUT );
		$Info = $Query->Query( );
		if( $Info === false ){
			$Query->Close( );
			$Query->Connect( );
			$Info = $Query->QueryOldPre17( );
		}
	} catch( MinecraftPingException $e ) {
		$Exception = $e;
	}

	if( $Query !== null ){
		$Query->Close();
	}
	$Timer = Number_Format( MicroTime( true ) - $Timer, 4, '.', '' );
?>
<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="cache-control" content="no-cache, must-revalidate">
        <meta name="keywords" content="The Crafters Network, The Crafters, Crafters Network, Crafters, Network, Darkcop, AveryThomas, Avery Thomas, Avery, Thomas, Dark, Cop, thecrafters.net, averythomas.com, darkcop.co.uk, store.thecrafters.net, forums, The Crafters Network Forum, Vote, Forums, The Crafters Network Vote, minecraft, server">
        <meta name="description" content="The Crafters Network Minecraft Server Network">
        <title>Portal - The Crafters Network</title>       
       
        <link rel="stylesheet" type="text/css" href="Landing/css/style.css" />
        <link rel="stylesheet" type="text/css" href="Landing/css/animate.css" />
        <link rel="stylesheet" type="text/css" href="Landing/css/buttons.css" />
        <link rel="stylesheet" href="Landing/css/font-awesome.min.css">
        <link rel="icon" type="image/png" href="Landing/Landing/images/favicon.png" />

        <script type='text/javascript' src="Landing/js/jquery.min.js"></script>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
       <style>.pulsefade {-webkit-animation: fade 1s 750ms 1 ease, pulse 2s 1s infinite ease-out;}</style>
    </head>
    <body>
        <div class="wrapper">
            <a href="#"><img style="margin-top: 30px;" class="pulsefade" src="https://thecrafters.net/forum/images/logo.png" alt="The Crafters Network Logo"/></a>
            <div class="hr"></div>
            <br>
            <?php
            if($Info === true) {
              echo '<h2>Network Offline <font style="color: red;"><i class="fa fa-arrow-circle-o-down"></i></font></h2>';
            } else {
              echo '<h2>Network Online <font style="color: green;"><i class="fa fa-arrow-circle-o-up"></i></font></h2>';
            }
            ?>
            <br>

            <a href="https://thecrafters.net/forum/" class="DemButtons"><i class="fa fa-globe"></i> Forums</a>
            <a href="http://store.thecrafters.net/" class="DemButtons"><i class="fa fa-shopping-cart"></i> Store</a>
            <a href="https://thecrafters.net/forum/pages/vote/" class="DemButtons"><i class="fa fa-heart"></i> Vote</a>
            <a href="https://thecrafters.net/bans/" class="DemButtons"><i class="fa fa-ban"></i> Bans</a>
        <footer style="margin-top: 100px;">
            <center><font color="grey">We are not affiliated with Mojang AB or Minecraft.net</center>
        </footer>
      </div >
    </body>
</html>